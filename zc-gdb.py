import re
import gdb

def token_str(v):
	r = str(v['t'])
	if(r == "T_INT"):
		r += ": " + str(v['intv'])
	elif(r == "T_REAL"):
		r += ": " + str(v['realv'])
	elif(r == "T_TYPE"):
		r += ": " + str(v['typev'])[1:]
	elif(r == "T_IDENT"):
		r += ": " + v['strv'].string()
	return r

class TokenLP:
	def __init__(self, val):
		self.val = val

	@staticmethod
	def vtos(v):
		r = token_str(v)
		n = v['n']
		if(int(n) == 0):
			return r
		return r + "\n" + TokenLP.vtos(n)

	def to_string(self):
		return TokenLP.vtos(self.val)

class TokenP:
	def __init__(self, val):
		self.val = val

	def to_string(self):
		return token_str(self.val)

class AstP:
	def __init__(self, val):
		self.val = val

	@staticmethod
	def childrenstr(v):
		c = v
		r = ""
		while(int(c) != 0):
			r += " " + AstP.dispatch(c)
			c = c['n']
		return r

	@staticmethod
	def dispatch(v):
		t = str(v['at'])
		if(t == "A_PROGRAM"):
			return "(program" + AstP.childrenstr(v['c']) + ")"
		if(t == "A_STLIST"):
			return "(statement-list" + AstP.childrenstr(v['c']) + ")"
		if(t == "A_RETURN"):
			if(int(v['c']) == 0): return "(return)"
			return "(return " + AstP.dispatch(v['c']) + ")"
		if(t == "A_INT"):
			return "(int " + str(v['intv']) + ")"
		if(t == "A_REAL"):
			return "(real " + str(v['realv']) + ")"
		if(t == "A_IDENT"):
			return "(ident " + v['strv'].string() + ")"
		if(t == "A_DEFAULTDEC"):
			return "(default-dec" + AstP.childrenstr(v['c']) + ")"
		if(t == "A_DEFINEDDEC"):
			return "(defined-dec" + AstP.childrenstr(v['c']) + ")"
		if(t == "A_LAMBDA"):
			return "(lambda" + AstP.childrenstr(v['c']) + ")"
		if(t == "A_CALL"):
			return "(call" + AstP.childrenstr(v['c']) + ")"
		return "(UNKNOWN " + t + ")"

	def to_string(self):
		return AstP.dispatch(self.val)

class ExptypeP:
	def __init__(self, val):
		self.val = val

	@staticmethod
	def vtos(v):
		r = ""
		if(v["m"].string() == "f"): r+="()"
		r+=str(v["b"])[1:]
		return r

	def to_string(self):
		return ExptypeP.vtos(self.val)

class SymbolP:
	def __init__(self, val):
		self.val = val

	@staticmethod
	def vtos(v):
		return v['n'].string() + ": " + ExptypeP.vtos(v['t'])

	def to_string(self):
		return SymbolP.vtos(self.val)

class ScopeP:
	def __init__(self, val):
		self.val = val

	@staticmethod
	def vtos(v):
		r = ""
		sym = v["s"]
		while(int(sym) != 0):
			r += SymbolP.vtos(sym) + "\n"
			sym = sym["ns"]
		return r

	def to_string(self):
		return ScopeP.vtos(self.val)

class FullScopeP:
	def __init__(self, val):
		self.val = val

	@staticmethod
	def recurse(v):
		if(int(v) == 0): return ""
		return FullScopeP.recurse(v["p"]) + ScopeP.vtos(v)

	def to_string(self):
		return FullScopeP.recurse(self.val)
			
def lookup(val):
	type = val.type.strip_typedefs()
	if type.code == gdb.TYPE_CODE_PTR:
		tag = type.target().strip_typedefs().tag
		if tag == "token":
			return TokenLP(val)
		if tag == "astnode":
			return AstP(val)
		if tag == "scope":
			return FullScopeP(val)
		if tag == "symbol":
			return SymbolP(val)
		if tag == "exptype":
			return ExptypeP(val)
	if type.code == gdb.TYPE_CODE_STRUCT:
		tag = type.tag
		if tag == "token":
			return TokenP(val)
		if tag == "astnode":
			return AstP(val)
		if tag == "scope":
			return ScopeP(val)
		if tag == "symbol":
			return SymbolP(val)
		if tag == "exptype":
			return ExptypeP(val)
	return None

gdb.pretty_printers.append(lookup)
