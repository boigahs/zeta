# Zeta
A compiler for the Zeta programming language.
The language is heavily based on C, and documentation will often speak in relative terms to C.
The intent is to be similar to C, but allow for experimenting with new language features.

This project uses Plan 9 mkfiles, **not** GNU or any other type of Makefile. You will need [Plan 9 From User Space](https://9fans.github.io/plan9port/).

For planned features, refer to _ideas.md_
