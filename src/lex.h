/*
#include <stdint.h>
#include "lex.h"
*/

typedef enum basetype {
	Bvoid,
	Bs64,
	Bf64,
} basetype;

typedef enum ttype {
	EOTL,
	T_INT,
	T_REAL,
	T_TYPE,
	T_IDENT,
	T_RETURN,
	T_AUTO,
	T_ARROW,
	T_SEMIC = ';',
	T_ASSIGN = '=',
	T_MINUS = '-',
	T_LPAREN = '(',
	T_RPAREN = ')',
	T_LBRACE = '{',
	T_RBRACE = '}',
} ttype;

typedef struct token {
	ttype t;
	union {
		int64_t intv;
		double realv;
		basetype typev;
		char *strv;
	};
	struct token *n;
} token;

token *tokenize(char *);
