#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "lex.h"
#include "parse.h"

scope top;
scope *current = &top;

void
newscope(void){
	scope *s = calloc(1, sizeof(scope));
	s->p = current;
	current = s;
}

scope *
popscope(void){
	scope *s = current;
	assert(current->p);
	current = current->p;
	return s;
}

void
setscope(scope *s){
	current = s;
}

void
addsymbol(char *n, basetype b, char *m){
	symbol **s;
	if(b == Bvoid) assert(*m != '\0');
	for(s=&current->s; *s; s=&((*s)->ns));
	*s = calloc(1, sizeof(symbol));
	(*s)->n = strdup(n);
	(*s)->t.b = b;
	(*s)->t.m = strdup(m);
}

symbol *
getsym(char *n){
	scope *scope;
	symbol *s;
	for(scope=current; scope; scope=scope->p){
		for(s=scope->s; s; s=s->ns) if(!strcmp(n, s->n)) return s;
	}
	assert(s);
	return NULL;
}
