/*
#include <stdint.h>
#include "lex.h"
#include "parse.h"
*/

typedef struct exptype {
	basetype b;
	char *m;
	uint8_t literal;
} exptype;

/* symtab.c */
typedef struct symbol {
	char *n;
	exptype t;
	void *llvar;
	void *llcval;
	struct symbol *ns;
} symbol;

typedef struct scope {
	symbol *s;
	struct scope *p;
} scope;

void newscope(void);
scope *popscope(void);
void setscope(scope *);
void addsymbol(char *n, basetype t, char *modifiers);
symbol *getsym(char *n);

/* parse.c */
typedef enum asttype {
	A_PROGRAM,
	A_STLIST,
	A_INT,
	A_REAL,
	A_RETURN,
	A_DEFAULTDEC,
	A_DEFINEDDEC,
	A_IDENT,
	A_LAMBDA,
	A_CALL,
} asttype;

typedef struct astnode {
	asttype at;
	exptype et;
	union {
		int64_t intv;
		double realv;
		char *strv;
		basetype typev;
		scope *scope;
	};
	struct astnode *c;
	struct astnode *n;
} astnode;

astnode *parse(token *);
astnode *getchild(astnode *p, int i);

