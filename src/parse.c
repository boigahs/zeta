#include <assert.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "lex.h"
#include "parse.h"

/* Forward declarations for recursion */
static astnode *returnst(void);
static astnode *declaration(void);

/* Tokens */
token *ctoken;

static void
next(void){
	ctoken = ctoken->n;
}

static int
istype(ttype t){
	return ctoken->t == t;
}

static int
accept(ttype t){
	if(ctoken->t == t){
		next();
		return 1;
	}
	return 0;
}

/* AST */
#define node() calloc(1, sizeof(astnode))

astnode *
getchild(astnode *p, int i){
	astnode *r = p->c;
	for(; i>0; i--){
		if(!r) return NULL;
		r=r->n;
	}
	return r;
}

static int
addchild(astnode *p, astnode *c){
	astnode *n;
	if(!c) return 0;
	if(!p->c){
		p->c = c;
		return 1;
	}
	for(n=p->c; n->n; n=n->n);
	n->n = c;
	return 1;
}

static astnode *
number(void){
	astnode *r;
	int s = accept('-') ? -1 : 1;
	int i = istype(T_INT);

	if(!(i || istype(T_REAL))) return NULL;
	r = node();
	if(i){
		r->at = A_INT;
		r->et.b = Bs64;
		r->et.m = strdup("");
		r->intv = s*ctoken->intv;
	}
	else {
		r->at = A_REAL;
		r->et.b = Bf64;
		r->et.m = strdup("");
		r->realv = s*ctoken->realv;
	}
	next();
	r->et.literal = 1;
	return r;
}

basetype
gettypename(void){
	basetype r;
	assert(istype(T_TYPE));
	r = ctoken->typev;
	next();
	return r;
}

static astnode *
identifier(void){
	astnode *r;
	if(!istype(T_IDENT)) return NULL;
	r = node();
	r->at = A_IDENT;
	r->strv = strdup(ctoken->strv);
	next();
	return r;
}

static astnode *
statementlist(void){
	astnode *r;
	if(!accept( '{' )) return NULL;
	r = node();
	r->at = A_STLIST;
	while(addchild(r, declaration()));
	assert(addchild(r, returnst()));
	assert(accept( '}' ));
	return r;
}

static astnode *
lambda(void){
	astnode *r, *sl;
	basetype t;
	if(!accept( '(' )) return NULL;
	assert(accept( ')' ));
	assert(accept(T_ARROW));
	t=gettypename();
	newscope();
	assert(sl=statementlist());
	r = node();
	r->at = A_LAMBDA;
	r->et.b = t;
	r->et.m = strdup("f");
	r->et.literal = 1;
	r->c = sl;
	r->scope = popscope();
	return r;
}

static astnode *
literalexp(void){
	astnode *r;
	if(r=number());
	else if(r=lambda());
	else if(r=identifier()){
		symbol *s = getsym(r->strv);
		r->et.b = s->t.b;
		r->et.m = strdup(s->t.m);
	}
	return r;
}

static astnode *
call(void){
	astnode *r, *f;
	if(!(f=literalexp())) return NULL;
	if(!accept( '(' )) return f;
	assert(accept( ')' ));
	assert(*f->et.m == 'f');
	r = node();
	r->at = A_CALL;
	r->et.b = f->et.b;
	r->c = f;
	return r;
}

static astnode *
expression(void){
	return call();
}

char *
modifiers(void){
	char *r;
	if(accept( '(' )){
		assert(accept(')'));
		r = calloc(2, 1);
		r[0] = 'f';
	}
	else {
		r = calloc(1, 1);
	}
	return r;
}

static astnode *
declaration(void){
	astnode *r, *i;
	basetype t;
	char *m;

	if(!(i=identifier())) return NULL;
	r = node();
	addchild(r, i);
	m = modifiers();
	if(accept('=')){
		astnode *e;
		r->at = A_DEFINEDDEC;
		assert(e = expression());
		addchild(r, e);
		if(accept(T_AUTO)){
			assert(*m == '\0');
			assert(e->et.literal);
			t = e->et.b;
			m = strdup(e->et.m);
		}
		else {
			t=gettypename();
			assert(e->et.b == t);
			assert(!strcmp(e->et.m, m));
		}
	}
	else {
		r->at = A_DEFAULTDEC;
		t=gettypename();
	}
	assert(accept(';'));
	addsymbol(i->strv, t, m);
	free(m);
	return r;
}

static astnode *
returnst(void){
	astnode *r;
	if(!accept(T_RETURN)) return NULL;
	r = node();
	r->at = A_RETURN;
	r->c=expression();
	assert(accept( ';' ));
	return r;
}

astnode *
parse(token *in){
	astnode *r;

	ctoken = in;
	r = node();
	r->at = A_PROGRAM;
	assert(addchild(r, declaration()));
	while(!istype(EOTL)) assert(addchild(r, declaration()));
	return r;
}
