#include <llvm-c/Core.h>
#include <llvm-c/Analysis.h>
#include <llvm-c/BitWriter.h>
#include <assert.h>
#include <stdio.h>
#include <stdint.h>
#include "lex.h"
#include "parse.h"

LLVMModuleRef mod;

/* Forward declarations */
static LLVMValueRef dispatch(astnode *, LLVMBuilderRef);

static LLVMTypeRef
ztolltype(exptype *t){
	LLVMTypeRef r;
	switch(t->b){
	case Bvoid:
		r = LLVMVoidType();
		break;
	case Bs64:
		r = LLVMInt64Type();
		break;
	case Bf64:
		r = LLVMDoubleType();
		break;
	default:
		assert(!"bad basetype");
		break;
	}
	if(*t->m == 'f') r = LLVMFunctionType(r, NULL, 0, 0);
	return r;
}

static LLVMValueRef
ztypedefinit(exptype *et){
	assert(et->b != Bvoid);
	assert(*et->m == '\0');
	switch(et->b){
	case Bs64:
		return LLVMConstInt(LLVMInt64Type(), 0, 0);
	case Bf64:
		return LLVMConstReal(LLVMDoubleType(), 0.0);
	}
}

static LLVMValueRef
constvalue(astnode *a){
	assert(a->et.b != Bvoid);
	switch(a->at){
	case A_INT:
		return LLVMConstInt(LLVMInt64Type(), a->intv, 1);
	case A_REAL:
		return LLVMConstReal(LLVMDoubleType(), a->realv);
	case A_IDENT:
		return getsym(a->strv)->llcval;
	}
	return NULL;
}

static LLVMValueRef
lambda(astnode *node){
	astnode *st;
	LLVMTypeRef ft = ztolltype(&node->et);
	printf("type: %s\n", LLVMPrintTypeToString(ft));
	LLVMValueRef f = LLVMAddFunction(mod, "lambda", ft);
	LLVMBasicBlockRef ent = LLVMAppendBasicBlock(f, "entry");
	LLVMBuilderRef b = LLVMCreateBuilder();
	LLVMPositionBuilderAtEnd(b, ent);
	setscope(node->scope);
	for(st=node->c->c; st; st=st->n) dispatch(st, b);
	popscope();
	LLVMDisposeBuilder(b);
	return f;
}

static LLVMValueRef
call(astnode *node, LLVMBuilderRef bld){
	LLVMValueRef f = dispatch(node->c, bld);
	return LLVMBuildCall(bld, f, NULL, 0, "call");
}

static void
localdec(astnode *node, LLVMBuilderRef bld){
	symbol *s = getsym(getchild(node, 0)->strv);
	if(node->at == A_DEFINEDDEC){
		astnode *v = getchild(node, 1);
		s->llcval = dispatch(v, bld);
	}
	else if(node->at == A_DEFAULTDEC){
		assert(*s->t.m == '\0');
		s->llcval = ztypedefinit(&s->t);
	}
}

static LLVMValueRef
dispatch(astnode *node, LLVMBuilderRef bld){
	switch(node->at){
	case A_RETURN:
		return node->c ? LLVMBuildRet(bld, dispatch(node->c, bld)) : LLVMBuildRetVoid(bld);
	case A_INT:
	case A_REAL:
	case A_IDENT:
		return constvalue(node);
	case A_LAMBDA:
		return lambda(node);
	case A_CALL:
		return call(node, bld);
	case A_DEFAULTDEC:
	case A_DEFINEDDEC:
		localdec(node, bld);
		return NULL;
	}
	return NULL;
}

static void
globaldec(astnode *node){
	LLVMValueRef i;
	char *n = node->c->strv;
	symbol *s = getsym(n);
	exptype *t = &getsym(n)->t;

	if(node->at == A_DEFAULTDEC) i = ztypedefinit(t);
	else if(node->at == A_DEFINEDDEC) i = dispatch(getchild(node, 1), NULL);
	else return;

	LLVMValueRef c = LLVMAddGlobal(mod, LLVMTypeOf(i), n);
	LLVMSetInitializer(c, i);
	s->llcval = i;
	LLVMSetGlobalConstant(c, 1);
	s->llvar = c;
}

void
llvmgen(char *fn, astnode *ast){
	astnode *a;
	char *error = NULL;

	mod = LLVMModuleCreateWithName("module");

	assert(ast->at == A_PROGRAM);
	for(a = ast->c; a; a=a->n) globaldec(a);

	LLVMTypeRef ft = LLVMFunctionType(LLVMVoidType(), NULL, 0, 0);
	LLVMValueRef f = LLVMAddFunction(mod, "main", ft);
	LLVMBasicBlockRef ent = LLVMAppendBasicBlock(f, "entry");
	LLVMBuilderRef b = LLVMCreateBuilder();
	LLVMPositionBuilderAtEnd(b, ent);
	LLVMBuildCall(b, getsym("start")->llcval, NULL, 0, "");
	LLVMBuildRetVoid(b);
	LLVMDisposeBuilder(b);

	LLVMVerifyModule(mod, LLVMAbortProcessAction, &error);
	LLVMDisposeMessage(error);
	if(LLVMWriteBitcodeToFile(mod, fn)) printf("Error writing bitcode to file: %s\n", fn);
}
