#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include "lex.h"
#include "parse.h"

void llvmgen(char *, astnode *);

int
main(int argc, char **argv){
	astnode *n;
	token *t;
	if(argc!=2) return -1;
	t = tokenize(argv[1]);
	if(!t) return 1;
	n = parse(t);
	if(!n) return 2;
	llvmgen("test.bc", n);
	system("llvm-dis test.bc -o test.ll");
	system("llc test.bc -o test.asm");
	system("gcc -x assembler test.asm -o o.test");
	return 0;
}
