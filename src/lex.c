#include <ctype.h>
#include <errno.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "lex.h"

#define cstrlen(s) (sizeof(s)-1)
#define cstrcmp(v, c) strncmp(v, c, cstrlen(c))
#define returnif(e) if(r=(e)) return r

char *cc;
token *ct;

static void
die(char *m){
	puts(m);
	exit(1);
}

static token *
done(void){
	token *old = ct;
	ct->n = calloc(1, sizeof(token));
	ct = ct->n;
	return old;
}

static token *
istype(void){
	int l;
	basetype v;

	if(!cstrcmp(cc, "s64")) v=Bs64, l=3;
	else if(!cstrcmp(cc, "f64")) v=Bf64, l=3;
	else if(!cstrcmp(cc, "void")) v=Bvoid, l=4;
	else return NULL;
	cc += l;
	ct->t = T_TYPE;
	ct->typev = v;
	return done();
}

static token *
isnumber(void){
	char *e;

	if(!isdigit(*cc)) return NULL;
	ct->intv = strtol(cc, &e, 10);
	if(*e == '.'){
		if(isdigit(*(e+1))){
			ct->realv = strtod(cc, &e);
			ct->t = T_REAL;
		}
		else die("Improper number constant.");
	}
	else ct->t = T_INT;
	cc = e;
	return done();
}

static token *
isident(void){
	size_t l;
	char *c = cc;
	while(isalnum(*c)) c++;
	l = c-cc;
	ct->strv = strndup(cc, l);
	ct->t = T_IDENT;
	cc=c;
	return done();
}

static token *
ischartoken(char c){
	if(*cc != c) return NULL;
	ct->t = c;
	cc++;
	return done();
}

static token *
isstringtoken(char *s, ttype t){
	size_t l = strlen(s);
	if(strncmp(cc, s, l)) return NULL;
	cc += l;
	ct->t = t;
	return done();
}

static token *
iskeyword(void){
	token *r;
	returnif(isstringtoken("return", T_RETURN));
	returnif(isstringtoken("auto", T_AUTO));
	return NULL;
}

token *
tokenize(char *in){
	token *r = calloc(1, sizeof(token));
	ct = r;
	cc = in;

	while(*cc){
		while(isblank(*cc)) cc++;
		if(isalpha(*cc)){
			if(iskeyword()) continue;
			if(istype()) continue;
			if(isident()) continue;
			else die("Invalid token.");
		}
		else if(isnumber()) continue;
		else if(*cc =='-'){
			if(isstringtoken("->", T_ARROW)) continue;
			else ischartoken('-');
			continue;
		}
		else if(ischartoken(';')) continue;
		else if(ischartoken('=')) continue;
		else if(ischartoken('(')) continue;
		else if(ischartoken(')')) continue;
		else if(ischartoken('{')) continue;
		else if(ischartoken('}')) continue;
		else die("Invalid token.");
	}
	return r;
}
