# Overview
## Core Features
* First class functions, lambdas, and nested function definitions.
* Concurrency integrated into the language with operators
* [Plan 9 extensions](http://doc.cat-v.org/plan_9/4th_edition/papers/comp) built in.
* Reworked operators to reduce precedence issues
  * Number of prefix operators kept to a minimum (- and !).
  * Postfix operators are evaluated from left to right.
  * New inc/dec ops: +?, ?+, -?, ?-.
* No multidimensional arrays. They are simply normal arrays that are accessed a little differently.
* New typenames.
* UTF8 only.

## Operator Precedence Table
Issue with BitBucket means I can't display the "|" character in this table. Pipe means "|" and Double Pipe means "||"

| Precedence | Operator | Description | Associativity |
|:--------:|:-------:|:---------:|:----------:|
| 1 | @ | Dereference | Postfix |
|  | $ | 	Address-of | |
|  | # | Channel access | |
|  | ?+ ?- | Get value, then increment/decrement | |
|  | +? -? | Increment/decrement, then get value | |
|  | () | Function call | |
|  | [] | Array subscripting | |
|  | . | Struct/union member access | |
| 2 | (type) | Cast | Prefix |
|  | - | Unary minus | |
|  | ! ~ | Logical/Bitwise NOT | |
|  | sizeof | Size of | |
| 3 | * / % | Multiplication, Division, Remainder | Left to Right |
| 4 | + - | Addition, Subtraction | |
| 5 | < > <= >= | Relational operators | |
| 6 | == != | Equivalence operators | |
| 7 | & | Bitwise AND | |
| 8 | Pipe | Bitwise OR | |
|  9 | && | Logical AND | |
| 10 | Double Pipe | Logical OR | |
| 11 | = | Assignment | Right to Left |
| 12 | , | Comma | Left to Right |

## Examples and Explanations
### Declarations
At first glance, they may look like Go's declarations, but they are really more in line with C's.
Zeta retains the relationship between declarations and expressions, and also allows declaring different types of variables as long as the base type is the same.
```
/* x is a pointer to an int, because x@ evaluates to an int */
x@ int; 
/* function declarations work the same way */
x() float;
/* x is a function that returns a pointer to a byte */
x()@ byte 

/* You can mix and match like in C */
c, p@, f() byte 

 /* Initialization is the same as C as well */
x = 0, y = 5 int;
/* Declaring and initializing functions at once looks kind of strange at first */
f()@ = (void)->(@byte){ /*body*/ } byte;
/* Just use auto */
f = (void)->(@byte){ /*body*/ } auto;
```

### Postfix operators
No more _->_!
```
/* Dereference pointer, access 'm' member of struct */
sp@.m;
/* Access 'mp' member of struct, then dereference */
s.mp@;
```
Complicated expressions become very simple to read
```
/* Dereference foo, access member bar, call bar, access i member of return value. Try writing out the same thing in C. */
foo@.bar()[i];
/* NOT a multidimensional array! This is two array accesses. Helpful for arrays of arrays*/
entries[i][j];
```

### Multidimension arrays
Multidimensional arrays are just arrays. They do not have any sizing information.
Much like the one-dimensional array access, it is simply a shortcut for pointer arithmetic.
```
byte array[] = calloc(1, 8*8*8);
/* The following are all equivalent */
array[z:h, y:w, x];
array[x, y:w, z:h];
(array + x + y*w + z*w*h)@;
```

## Heavily considered
### Lambda syntax
* ```(args)->returntype {body};```
    * normal closure, captures all variables in scope
* ```(args)->returntype (capturelist){body};```
    * "reverse" closure, only captures specified variables
* ```(args)->returntype (){body};```
    * no closure, captures no variables. just a locally defined function.
* an empty list of args is equivalent to C's "void" arg list.

### Arrays
* Arrays are just an allocation method. There is no array type.
* Arrays can be used to initialize pointers and slices
* Slices
    * A pointer with length (wide pointer)
    * Bounds checking done implicitly
    * lengthof operator to get length
    * Slices automatically get casted to pointers when necessary
    * New slice arithmetic
        * slice + int = slice with its start moved upwards (length adjusted)
        * slice - int = slice with its length reduced by int
        * slice - slice = subsection of the two slices (maybe not really helpful. implement only once needed)
    * You can only slice smaller. Cannot expand.
        * You can cheat by casting to pointer, but we'll trust you know what you're doing.
        * I never really liked the term slice, but it explains this behavior concisely.
* Pointers
    * New arithmetic to convert to slice: pointer * integer converts the pointer to a slice of length integer
    * Otherwise exactly identical to C pointers
* Will need to re-think multidimensional arrays
### Option types
* Makes the "null" concept more universal
* Syntax doesn't have to be a pain (see zig, can be improved)
* Makes optional parameters very easy
* No real overhead for pointers and slices (0x0, see zig), and maybe floats (NaN)
* Gotta find a good operator symbol for it, kinda running out.
    * Maybe postfix !. Shouldn't cause parsing problems...
### Polymorphic functions.
* All variations of a function have to be part of the same definition, in the same place. No overloading someone else's function.
* The above, strong typing, and no inheritance or implicit casts/conversions mean that exactly what code is being run should be easily understood by both programmer and compiler.
### More powerful const
* You cannot turn something that is not constant into a constant later.
* You cannot cast off constant ever.
* New declaration syntax makes it clear which parts are constant.
    * i.e. ```p const@ int;``` vs ```i@ const int;```
* Unfortunately cannot protect from memory related undefined behavior. It should be very uncommon, though.
* Fully compile time constants would be ideal, but then pointers to them lose meaning.
    * Also provide a scoped #define?
### Initializers
* Default declaration initializes to 0 when possible
    * Pointers and slices have no sensible default. The compiler will stop any use of uninitialized pointers/slices.
* Keyword used in defined declarations to indicate uninitialized allocation.
* Structs can have their initialization values defined.
    * Also provide const struct fields that cannot be changed from their default.
        * How should that interact with an uninitialized struct?
### then clause
* Runs if any previous if/else/for before it evaluated. Does not run otherwise.
* Should be easily representable in llvm's block control.
* Can use multiple thens in a single chain
    * i.e. if else then else else then, the second then only applies to the last two elses.
### Compiler errors
* Only shows first error then stops.
* No warnings at all.
* Provide "lint" type thing that will scan your program and give you warnings.

## Under consideration
* Promotion of anonymous substructs requires explicit cast, I.E. 

```
struct inner { a int; };
struct outer { n@ char; inner; };
extern foo(x@ inner) void;
bar(y@ outer) void;
bar = lambda(void)(y@ outer){ foo((@inner) y) };
/* The above passes a pointer to the anonymous inner within y. It is not a normal pointer cast */
/* Not sure what to do if you actually do want a normal cast. Maybe an intermediate cast to void, or just use a separate syntax for promotion.*/
```

* Be more strict about extern usage. Every variable that is defined in another file has to be marked extern.
    * Variable keyword blocks to make this less annoying (i.e. every declaration in an "extern" block is extern)
* Improve #include
    * "Smart" include macro. Scans through .z files and creates "fake" headers based on public declarations. Package manager functionality without the stupidity.
    * Clang has some "module" thing. Look into that?
* Some sort of auto detection for function types to reduce typing
    * Auto for any type as long as the initializer is a constant expression (no user constants)
* Built-in fuzzy comparison operator for floating points
* % that works on floating points
* Operator shortcut for node = node@.next to make linked lists and trees simpler to use.
* Accessing unions by type names (could cause trouble with complex types)
* Macro(?) for getting the size of complex (string, array, struct) literals.
* go-like for loop
* jai-like #run directive
* using statement for importing structs into current scope
* Optional type and bounds checking for enums
* Built in vector/matrix/quaternion types with proper operators and functions that compile to SIMD instructions (glsl style) (swizzling!!!)
* Struct-of-array types
    * AOS: foo[5] bar;
    * SOA: foo bar[5];
    * Generic operator (i.e. [3:pos] or [pos:3]) for accessing SOA or AOS
    * Make it a storage specifier like in jai?
* Make struct member access also automatically dereference all the way down?
    * Same for channels and option types??
